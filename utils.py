import os
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image


def fft(x):
    return np.fft.fftshift(np.fft.fft2(x,norm="ortho"))

def ifft(x):
    return np.fft.ifft2(np.fft.ifftshift(x),norm="ortho")

def load_image(image_name,
             image_size,
             datadir="data/"):

    fullname = f"{image_name}{image_size}.png"
    img = Image.open(os.path.join(datadir,fullname))
    return np.asarray(img, dtype='float64')

def plot_gray(img,title="",figure=True):
    if figure:
        plt.figure()
    plt.imshow(img,cmap='Greys_r')
    plt.title(title)
    plt.show()

def subplots(imgtile,figsize=(7,7)):
    """
    img title: list of tuple: plot accordingly
    """
    n_plots = len(imgtile)
    xplots = int(np.floor(np.sqrt(n_plots)))
    yplots = int(np.ceil(np.sqrt(n_plots)))

    
    fig, axs = plt.subplots(xplots,yplots,figsize=figsize)
    for idx, ax in enumerate(axs.flat):
        ax.imshow(imgtile[idx][0],cmap="Greys_r")
        ax.set_title(imgtile[idx][1])
    plt.show()
    
def mask_reco(mri_img,kspace_mask, signoise=10):
    kspace_data = fft(mri_img)
    #add Gaussian complex-valued random noise
    signoise = 10
    kspace_data += np.random.randn(*mri_img.shape) * signoise * (1+1j)
    # Mask data to perform subsampling
    kspace_data *= kspace_mask
    kspace_data_abs = np.abs(kspace_data)
    # Zero order solution
    image_rec0 = ifft(kspace_data)
    subplots([(mri_img, "True Image"),
            (kspace_mask, "sampling mask"),
            (np.clip(kspace_data_abs,0,np.percentile(kspace_data_abs,99)),"kspace noisy, filtered data"),
            (np.abs(image_rec0), "Zero order rec.")])
    

############################
# Pysap import and utility #
############################
    
from mri.operators import NonCartesianFFT
from mri.operators.utils import convert_locations_to_mask, \
    gridded_inverse_fourier_transform_nd
from pysap.data import get_sample_data


from skimage import data, img_as_float, io, filters
from modopt.math.metrics import ssim


    
def non_cart_sampling(mri_img, kspace_loc, title, figsize=(10,10)):
    fig, axs = plt.subplots(1,2,figsize=figsize)
    #plot shots
    axs[0].scatter(kspace_loc[::4,0], kspace_loc[::4,1], marker = '.',s = 1)
    axs[0].set_title(title)
    axs[0].set(adjustable='box', aspect='equal')
    axs[0].grid()

    
    data=convert_locations_to_mask(kspace_loc, mri_img.shape)
    fourier_op = NonCartesianFFT(samples=kspace_loc, shape=mri_img.shape,
                                 implementation='cpu')
    kspace_obs = fourier_op.op(mri_img.data)
    grid_space = np.linspace(-0.5, 0.5, num=mri_img.shape[0])
    grid2D = np.meshgrid(grid_space, grid_space)
    grid_soln = gridded_inverse_fourier_transform_nd(kspace_loc, kspace_obs,
                                                     tuple(grid2D), 'linear')
    axs[1].imshow(np.abs(grid_soln), cmap='gray')
    # Calculate SSIM
    base_ssim = ssim(grid_soln, mri_img)
    axs[1].set_title(f'Gridded Solution\nSSIM = {base_ssim:.2f}')
    plt.show()   
